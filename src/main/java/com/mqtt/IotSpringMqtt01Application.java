package com.mqtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotSpringMqtt01Application {

	public static void main(String[] args) {
		SpringApplication.run(IotSpringMqtt01Application.class, args);
	}

}
