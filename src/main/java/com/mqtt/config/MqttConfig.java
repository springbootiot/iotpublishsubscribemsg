package com.mqtt.config;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.mqtt.MyMessage;
import com.mqtt.MyTopic;
import com.mqtt.model.Payloads;

@Configuration
public class MqttConfig  {

	String Clientendpoint = "...";
	String Clientid = "Client Id";
	String awsAccessKeyId = "Access key here";
	String awsSecretAccesskey = "AWS Secret Key here";
      
	AWSIotMqttClient client=null;
	MqttClient c1=null;
	public void connectToIot() throws AWSIotException {
	 client = new AWSIotMqttClient(Clientendpoint, Clientid, awsAccessKeyId, awsSecretAccesskey,
				null);
		client.connect();
	} 
	
	public void publish(Payloads payload) throws AWSIotException
	{
		String topic = "myTopic";
		AWSIotQos qos = AWSIotQos.QOS0;
		//String payload = "any payload";
		long timeout = 3000; // milliseconds

		MyMessage message = new MyMessage(topic, qos, payload.toString());
			client.publish(message, timeout);

	} 
	
	
	public void subscribe(String topic) throws AWSIotException
	{
		//String topicName = "myTopic";
		AWSIotQos qos = AWSIotQos.QOS0;

		MyTopic topicname = new MyTopic(topic, qos);
		client.subscribe(topicname);
		

	}
}