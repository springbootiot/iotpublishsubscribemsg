package com.mqtt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.iot.client.AWSIotException;
import com.mqtt.config.MqttConfig;
import com.mqtt.model.Payloads;

@RestController
public class Mqttcontroller {
	@Autowired
	MqttConfig mqttConfig;

	@RequestMapping("/pub")
	public String publish1(@RequestBody Payloads payload) throws AWSIotException {

		mqttConfig.connectToIot();
		System.out.println("connection established");
		mqttConfig.publish(payload);

		return "Connection established and Message Published";

	}
	
	@RequestMapping("/sub")
	public String subs(@RequestBody String topic) throws Exception {

		mqttConfig.connectToIot();
		System.out.println("Connection established");
		mqttConfig.subscribe(topic);
		
		System.out.println("Subscriptionm done");

		return "Connection established and Subscription done";

	} 
	

}
