package com.mqtt.model;

public class Payloads {
	private String sensor;

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	@Override
	public String toString() {
		return "WeatherPayload [sensor=" + sensor + "]";
	}

	
	
}
