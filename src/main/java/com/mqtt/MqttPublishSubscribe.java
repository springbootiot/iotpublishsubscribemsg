package com.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.amazonaws.services.iot.client.AWSIotException;
import com.mqtt.config.MqttConfig;
import com.mqtt.model.Payloads;

@Service
public class MqttPublishSubscribe {
	
	@Autowired
	MqttConfig mqttConfig;
	
	
	public void publishMsg(Payloads payload) throws AWSIotException
	{
		mqttConfig.connectToIot();
		mqttConfig.publish(payload);
		
	} 
	
	public void subsMsg(String topic) throws AWSIotException, MqttException
	{
		mqttConfig.connectToIot();
		mqttConfig.subscribe(topic);
		

		
		
	

		
	}

	

}
